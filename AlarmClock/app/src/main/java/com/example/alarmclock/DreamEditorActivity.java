package com.example.alarmclock;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.util.HashSet;

/**
 * In charge of the dream editor and commincates with the dream diary activity to update the dreams
 */
public class DreamEditorActivity extends AppCompatActivity {

    /**
     * unique identifier for notes entries to the list
     */
    int noteID;

    /**
     * Creates an array like structure and saves it
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_editor);

        EditText editText = (EditText)findViewById(R.id.editText);
        Intent intent = getIntent();
        noteID = intent.getIntExtra("noteID", -1);

        if(noteID != -1)
        {
            editText.setText(DreamDiaryActivity.notes.get(noteID));
        }

        else
        {
            DreamDiaryActivity.notes.add("");
            noteID = DreamDiaryActivity.notes.size() - 1;
            DreamDiaryActivity.arrayAdapter.notifyDataSetChanged();
        }

        editText.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            /**
             * If text changed then update the dream notes
             * @param s string of the notes
             * @param start Not Sure
             * @param before Not Sure
             * @param count Not Sure
             */
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                DreamDiaryActivity.notes.set(noteID, String.valueOf(s));
                DreamDiaryActivity.arrayAdapter.notifyDataSetChanged();

                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("DreamNotes", Context.MODE_PRIVATE);
                HashSet<String> set = new HashSet<>(DreamDiaryActivity.notes);
                sharedPreferences.edit().putStringSet("notes", set).apply();
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });
    }
}