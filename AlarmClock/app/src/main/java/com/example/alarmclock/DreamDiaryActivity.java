package com.example.alarmclock;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * In charge of the dream diary and updating removing and adding them
 */
public class DreamDiaryActivity extends AppCompatActivity {

    /**
     * notes is an array for dream notes
     * sleepSurvey is the button to the sleep survey
     * sleep graph is the button to sleep graph
     * twitter share is an api call to twitter
     */
    static ArrayList<String> notes = new ArrayList<String>();
    static ArrayAdapter<String> arrayAdapter;
    Button sleepSurvey;
    Button sleepGraph;
    Button twitterShare;

    /**
     * in charge of the menu
     * @param menu item
     * @return true or false
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_dream_note, menu);

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Will do something if a menu item is touches
     * @param item menu item touches
     * @return true of false
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        super.onOptionsItemSelected(item);

        if(item.getItemId() == R.id.add_dream_note)
        {
            Intent intent = new Intent(getApplicationContext(), DreamEditorActivity.class);
            startActivity(intent);
            return true;
        }

        return false;
    }

    /**
     * In charge of creating the notes and communicating with the editor to update the note page
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.diary);

        ListView listView = (ListView)findViewById(R.id.listView);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("DreamNotes", Context.MODE_PRIVATE);
        HashSet<String> set = (HashSet<String>)sharedPreferences.getStringSet("notes", null);


        if(set == null)
        {
            notes.add("Example Note");
        }

        else
        {
            notes = new ArrayList<>(set);
        }

        sleepGraph = findViewById(R.id.button3);
        sleepGraph.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SleepGraph.class);
                startActivity(intent);
            }
        });

        sleepSurvey = findViewById(R.id.button7);
        sleepSurvey.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SleepSurvey.class);
                startActivity(intent);
            }
        });

        twitterShare = findViewById(R.id.button8);
        twitterShare.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String tweetMessage = notes.get(0);
                Intent tweet = new Intent(Intent.ACTION_VIEW);
                tweet.setData(Uri.parse("http://twitter.com/?status=" + Uri.encode(tweetMessage)));
                startActivity(tweet);
            }
        });

        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, notes);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent intent = new Intent(getApplicationContext(), DreamEditorActivity.class);
                intent.putExtra("noteID", position);
                startActivity(intent);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id)
            {
                new AlertDialog.Builder(DreamDiaryActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Delete?")
                        .setMessage("Are you sure you want to delete this note?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                notes.remove(position);
                                arrayAdapter.notifyDataSetChanged();

                                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("DreamNotes", Context.MODE_PRIVATE);
                                HashSet<String> set = new HashSet<>(DreamDiaryActivity.notes);
                                sharedPreferences.edit().putStringSet("notes", set).apply();
                            }
                        })

                        .setNegativeButton("No", null)
                        .show();

                return true;
            }
        });
    }
}
