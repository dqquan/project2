package com.example.alarmclock;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.util.Timer;
import java.util.TimerTask;

/**
 * This activity is in charge of the alarm ato wake someone up
 */
public class AlarmActivity extends AppCompatActivity {

    /**
     * snooze checks if snooze is pressed
     * stop checks if stop button has been pressed
     * days checks how many sleep entries there are
     * timeSlept checks how many hours you slept total for the day
     */
    boolean snooze = false;
    boolean stop = false;
    boolean snooze2 = false;
    boolean changeActivity = false;
    Button snoozeButton;
    Button stopButton;
    String value;
    int timeSlept;
    int delay = 10;
    int i = 0;
    int days = 0;


    /**
     * Creates the dropdown menu
     * @param menu menu object
     * @return true of false
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //&& i = 3
        if(timeSlept < 8) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.tip_menu, menu);
        }
        return true;
    }

    /**
     * Creates tips when clicking on tips
     * @param item menu item clicked
     * @return true or false
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //&& i = 3
        if(timeSlept < 8) {
            switch (item.getItemId()) {
                case R.id.item1:
                    Toast.makeText(this, "Having been sleeping well? Here's some tips", Toast.LENGTH_SHORT);
                    return true;
                case R.id.item2:
                    Toast.makeText(this, "Try meditating before sleeping", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.item3:
                    Toast.makeText(this, "Try turning all blue lights off 1 hour before sleeping", Toast.LENGTH_SHORT).show();
                    return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * In charge of setting the timer and notification
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alarm);
        MediaPlayer alarmMp4 = null;
        boolean defaultAlarm = false;

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("TimesDeprivedOfSleep", Context.MODE_PRIVATE);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            timeSlept = extras.getInt("Time Slept");
            days = extras.getInt("Days Till Month");
        }
        if(timeSlept< 8) {
            i++;
        }

        days++;

        sharedPreferences.edit().putInt("SleptBelowEight", i);

//        if(days >= 30) {
//            days = 0;
//            sharedPreferences.edit().putInt("Days Till Month", days);
//        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(AlarmActivity.this, "MyNotification");
        builder.setContentTitle("Sleep Progress");
        builder.setContentText("Congrats you slept " + String.valueOf(timeSlept) + " more");
        builder.setSmallIcon(R.drawable.ic_launcher_background);
        builder.setAutoCancel(true);

        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(AlarmActivity.this);
        managerCompat.notify(1, builder.build());

        if (extras != null) {
            value = extras.getString("Alarm");
        }

        if(value.equals("Default Alarm")) {
            defaultAlarm = true;
        }
        else if(value.equals("Default Alarm 2")) {
            alarmMp4 = MediaPlayer.create(this, R.raw.alarm2);
        }
        else if(value.equals("Default Alarm 3")) {
            alarmMp4 = MediaPlayer.create(this, R.raw.alarm3);
        }

        snoozeButton = findViewById(R.id.button);
        stopButton = findViewById(R.id.button2);

        snoozeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                snooze = true;
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                stop = true;
            }
        });
        
        if(defaultAlarm == true) {
            final Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE));
            Timer t = new Timer();
            t.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    if (snooze == true) {
                        r.stop();
                    }
                    if(stop == true && changeActivity == false) {
                        r.stop();
                        changeActivity = true;
                        Intent i = new Intent(getApplicationContext(),
                                DreamDiaryActivity.class);
                        startActivity(i);
                    }
                    else if (snooze == false && stop == false){
                        r.play();
                    }
                }
            }, delay, 1000);
        }
        
        else if(alarmMp4 != null){
            Timer t = new Timer();
            MediaPlayer finalAlarmMp = alarmMp4;
            t.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    if (snooze == true) {
                        finalAlarmMp.stop();
                    }
                    if(stop == true && changeActivity == false) {
                        finalAlarmMp.stop();
                        changeActivity = true;
                        Intent i = new Intent(getApplicationContext(),
                                DreamDiaryActivity.class);
                        startActivity(i);
                    }
                    else if (snooze == false && stop == false){
                        finalAlarmMp.start();
                    }
                }
            }, delay, 1000);
        }



    }
}
