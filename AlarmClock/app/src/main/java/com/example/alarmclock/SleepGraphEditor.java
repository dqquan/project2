package com.example.alarmclock;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.util.HashSet;

/**
 * In charge of communicating with the SleepGraph activity to see if the user updated anything
 */
public class SleepGraphEditor extends AppCompatActivity {

    int noteID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graph_editor);

        EditText editText = (EditText)findViewById(R.id.editText);
        Intent intent = getIntent();
        noteID = intent.getIntExtra("noteID", -1);

        if(noteID != -1)
        {
            editText.setText(SleepGraph.notes.get(noteID));
        }

        else
        {
            SleepGraph.notes.add("");
            noteID = SleepGraph.notes.size() - 1;
            SleepGraph.arrayAdapter.notifyDataSetChanged();
        }

        editText.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            /**
             * Updates the sleep times when the user updates them in this activity
             * @param s
             * @param start
             * @param before
             * @param count
             */
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                SleepGraph.notes.set(noteID, String.valueOf(s));
                SleepGraph.arrayAdapter.notifyDataSetChanged();

                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("Hours", Context.MODE_PRIVATE);
                HashSet<String> set = new HashSet<>(SleepGraph.notes);
                sharedPreferences.edit().putStringSet("hours", set).apply();
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });
    }
}