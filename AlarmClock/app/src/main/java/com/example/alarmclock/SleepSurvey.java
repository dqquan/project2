package com.example.alarmclock;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * In charge of creating a sleep survey for the user to take
 */
public class SleepSurvey extends AppCompatActivity {

    /**
     * questioncounter checks how many questions it's been
     */
    String tipsAfterSurvey;
    int questionCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.survey);

        TextView question = findViewById(R.id.textView);

        Button answer1 = findViewById(R.id.button9);
        Button answer2 = findViewById(R.id.button10);
        Button answer3 = findViewById(R.id.button11);

        answer1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(questionCounter == 0) {
                    backSleeper();
                    questionCounter++;
                    question.setText("Do you look at your phone before falling asleep?");
                    answer1.setText("Yes");
                    answer2.setText("No");
                    answer3.setVisibility(View.GONE);
                }
                else if(questionCounter == 1) {
                    blueLightYes();
                    questionCounter++;
                    question.setText("How often do you move at night?");
                    answer1.setText("1-5");
                    answer2.setText("5-10");
                    answer3.setText("More than 10");
                    answer3.setVisibility(View.VISIBLE);
                }
                else if(questionCounter == 2) {
                    moveOneToFive();
                    Toast.makeText(getApplicationContext(), tipsAfterSurvey, Toast.LENGTH_LONG).show();
                }
            }
        });

        answer2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(questionCounter == 0) {
                    chestSleeper();
                    questionCounter++;
                    question.setText("Do you look at your phone before falling asleep?");
                    answer1.setText("Yes");
                    answer2.setText("No");
                    answer3.setVisibility(View.GONE);
                }
                else if(questionCounter == 1) {
                    blueLightNo();
                    questionCounter++;
                    question.setText("How often do you move at night?");
                    answer1.setText("1-5");
                    answer2.setText("5-10");
                    answer3.setText("More than 10");
                    answer3.setVisibility(View.VISIBLE);
                }
                else if(questionCounter == 2) {
                    moveFiveToTen();
                    Toast.makeText(getApplicationContext(), tipsAfterSurvey, Toast.LENGTH_LONG).show();
                }
            }
        });

        answer3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(questionCounter == 0) {
                    sideSleeper();
                    questionCounter++;
                    question.setText("Do you look at your phone before falling asleep?");
                    answer1.setText("Yes");
                    answer2.setText("No");
                    answer3.setVisibility(View.GONE);
                }
                else if(questionCounter == 1) {
//                    questionCounter++;
//                    question.setText("How often do you move at night?");
//                    answer3.setVisibility(View.GONE);
                }
                else if(questionCounter == 2) {
                    moveMoreThanTen();
                    Toast.makeText(getApplicationContext(), tipsAfterSurvey, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * These methods just append the tip that is associated with an answer
     */

    void backSleeper() {
        tipsAfterSurvey = "You should try putting a pillow between your back and leg \n";
    }

    void chestSleeper() {
        tipsAfterSurvey = "You should try putting a pillow in between your armpit \n";
    }

    void sideSleeper() {
        tipsAfterSurvey = "You should try to keep your shoulder from caving inwards \n";
    }

    void blueLightYes() {
        tipsAfterSurvey += "Try sleeping with out blue lights 1 hour before sleeping \n";
    }

    void blueLightNo() {
    }

    void moveOneToFive() {
    }

    void moveFiveToTen() {
        tipsAfterSurvey += "Try focusing on your breath when sleeping";
    }

    void moveMoreThanTen() {
        tipsAfterSurvey += "Try to relax and meditate before sleeping";
    }

}
