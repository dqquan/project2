package com.example.alarmclock;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * In charge of creating the graph using the hours set in the sleep graph activity
 */
public class GraphActivity extends AppCompatActivity {
    static ArrayList<String> notes = new ArrayList<String>();

    /**
     * Creates the graph using a GraphView
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graph);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("Hours", Context.MODE_PRIVATE);
        HashSet<String> set = (HashSet<String>)sharedPreferences.getStringSet("hours", null);

        if(set == null)
        {
            notes.add("Hours Slept: 3, Sleep Quality: 100%");
        }

        else
        {
            notes = new ArrayList<>(set);
        }

        GraphView graph = (GraphView) findViewById(R.id.idGraphView);

        DataPoint[] data = new DataPoint[notes.size()];

        try{
            for(int i =0; i<notes.size(); i++) {
                int x_axis = i;
                int y_axis = Integer.parseInt(String.valueOf(notes.get(i).charAt(13)));
                data[i] = new DataPoint(x_axis,y_axis);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(data);
        graph.addSeries(series);
    }
}
