package com.example.alarmclock;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextClock;
import android.widget.TimePicker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author david
 */

/**
 * In charge of creating the alarm clock and start and end times
 */
public class MainActivity extends AppCompatActivity {


    /**
     * set time is the first time it's used to compare the time and see the difference
     * mySpinner is the spinner menu
     */
    TimePicker alarmTime;
    TextClock currentTime;
    boolean alarm = false;

    boolean setTime = false;
    boolean setTime2 = false;

    String time1;
    String time2;
    String alarmType;

    Spinner mySpinner;

    Button setTimer;
    Button setTimer2;
    Button confirmTimer;

    private DocumentReference mDocRef = FirebaseFirestore.getInstance().document("sampleData/sleep");


    /**
     * Creates the notification and does the logic for the difference in time and chooses random time in between
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(MainActivity.this, "MyNotification");
        builder.setContentTitle("Time to sleep");
        builder.setContentText("Time to sleep");
        builder.setSmallIcon(R.drawable.ic_launcher_background);
        builder.setAutoCancel(true);

        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(MainActivity.this);

        setTimer = findViewById(R.id.button4);
        mySpinner = findViewById(R.id.spinner1);
        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1,
        getResources().getStringArray(R.array.alarms));

        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mySpinner.setAdapter(myAdapter);


        setTimer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setTime = true;
                time1 = AlarmTime();
            }
        });

        setTimer2 = findViewById(R.id.button5);

        setTimer2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setTime2 = true;
                time2 = AlarmTime();
            }
        });

        alarmTime = findViewById(R.id.timePicker);
        currentTime = findViewById(R.id.textClock);

        confirmTimer = findViewById((R.id.button6));
        confirmTimer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Random rand = new Random();
                int colonIndex = time1.indexOf(":");
                int timeOneHours = Integer.parseInt(time1.substring(0,colonIndex));

                int colonIndex2 = time2.indexOf(":");
                int timeTwoHours = Integer.parseInt(time2.substring(0,colonIndex2));

                if(time1.charAt(time1.length()-1) == 'P') {
                    timeOneHours += 12;
                }

                if(time2.charAt(time1.length()-1) == 'P') {
                    timeTwoHours += 12;
                }
                //This grabs the minutes because the end has a AM or PM
                int timeOneMinutes = Integer.parseInt(time1.substring(colonIndex + 1,time1.length()-3));
                int timeTwoMinutes = Integer.parseInt(time2.substring(colonIndex2 + 1,time1.length()-3));

                if(timeTwoMinutes<timeOneMinutes) {
                    timeTwoMinutes +=60;
                    timeTwoHours -= 1;
                }

                int differenceHours = timeTwoHours - timeOneHours;

                if(differenceHours < 8) {
                    managerCompat.notify(1, builder.build());
                }
                int differenceMinutes = timeTwoMinutes - timeOneMinutes;
                if(differenceHours == 0) {
                    alarmType = mySpinner.getSelectedItem().toString();

                    Timer t = new Timer();
                    t.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            if (currentTime.getText().toString().equals(AlarmTime()) && alarm == false && setTime == true && setTime2 == true){
                                alarm = true;
                                Intent i = new Intent(getApplicationContext(),
                                        AlarmActivity.class);
                                i.putExtra("Alarm", alarmType);
                                i.putExtra("Time Slept", differenceHours);
                                startActivity(i);
                            }
                        }
                    }, 0, 1000);
                }
                else {
                    int timeWakeUp = rand.nextInt()%differenceHours;
                    if(differenceMinutes == 0) {
                        differenceMinutes = 1;
                    }
                    int timeMinuteWakeUp = rand.nextInt()%differenceMinutes;
                    String timeWokenUp = null;

                    if(time1.substring(time1.length()-3, time1.length()) == "AM") {
                        timeWokenUp = String.valueOf(timeWakeUp) + ":" + String.valueOf(timeMinuteWakeUp) + "AM";
                    }
                    if(time1.substring(time1.length()-3, time1.length()) == "PM") {
                        timeWokenUp = String.valueOf(timeWakeUp) + ":" + String.valueOf(timeMinuteWakeUp) + "AM";
                    }

                    alarmType = mySpinner.getSelectedItem().toString();

                    Timer t = new Timer();
                    t.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            if (currentTime.getText().toString().equals(AlarmTime()) && alarm == false && setTime == true && setTime2 == true){
                                alarm = true;
                                Intent i = new Intent(getApplicationContext(),
                                        AlarmActivity.class);
                                i.putExtra("Alarm", alarmType);
                                i.putExtra("Time Slept", differenceHours);
                                startActivity(i);
                            }
                        }
                    }, 0, 1000);

                }

            }
        });

    }

    /**
     * Creates a formatted alarm time string
     * @return a formatted alarm time HH:MM
     */
    public String AlarmTime(){

        Integer alarmHours = alarmTime.getCurrentHour();
        Integer alarmMinutes = alarmTime.getCurrentMinute();
        String stringAlarmMinutes;

        if (alarmMinutes<10){
            stringAlarmMinutes = "0";
            stringAlarmMinutes = stringAlarmMinutes + alarmMinutes.toString();
        }else{
            stringAlarmMinutes = alarmMinutes.toString();
        }
        String stringAlarmTime;

        if(alarmHours>12){
            alarmHours = alarmHours - 12;
            stringAlarmTime = alarmHours.toString()+":"+stringAlarmMinutes+" PM";
        }
        else{
            if(alarmHours == 12 || alarmHours == 0) {
                stringAlarmTime = 12+":"+stringAlarmMinutes+" AM";
            }
            else {
                stringAlarmTime = alarmHours.toString()+":"+stringAlarmMinutes+" AM";
            }
        }
        return stringAlarmTime;
    }

}